//
//  MenuTableViewController.swift
//  demo-accordion-menu-ios
//
//  Created by Joshua de Guzman on 24/06/2018.
//  Copyright © 2018 jodeio. All rights reserved.
//

import UIKit
import AccordionMenuSwift

class MenuTableViewController: AccordionTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let profile = Parent(state: .collapsed, childs: ["Account Settings", "Transaction History"], title: "Profile")
        let settings = Parent(state: .collapsed, childs: ["Notification", "General Settings", "FAQ"], title: "Settings")
        let logout = Parent(state: .collapsed, childs: [], title: "Logout")
        
        dataSource = [profile, settings, logout]
        numberOfCellsExpanded = .several
        total = dataSource.count
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let (parent, isParentCell, actualPosition) = self.findParent(indexPath.row)
        
        print("actualPosition: \(actualPosition)")
        print("didSelectRowAt: \(indexPath.row)")
        
        guard isParentCell else {
            print("didSelectRowAt: childIndex -> \(indexPath.row)")
            
            // Add logic here (e.g. where to navigate when child menu is clicked)
            return
        }
        
        self.tableView.beginUpdates()
        self.updateCells(parent, index: indexPath.row)
        self.tableView.endUpdates()
    }
  
    override  open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
        
        let (parent, isParentCell, actualPosition) = self.findParent(indexPath.row)
        
        if !isParentCell {
            // Dequeue child custom view here
        }
        else {
            // Dequeue parent child custom view here
        }
        
        return cell
    }
    

}
